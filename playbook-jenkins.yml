---
- name: Install, configure, and run Jenkins in a Docker container
  hosts: all
  become: yes
  vars_files:
    - vars-jenkins.yml

  tasks:
    - name: Install required system packages
      apt:
        name: "{{ item }}"
        state: present
        update_cache: yes
      loop: "{{ required_packages }}"
      register: apt_result
      retries: 5
      delay: 5
      until: apt_result is succeeded

    - name: Install Java for Jenkins CLI
      apt:
        name: openjdk-11-jre-headless
        state: present
        update_cache: yes

    - name: Install Docker SDK for Python
      pip:
        name: docker
        state: present

    - name: Ensure Docker service is running
      service:
        name: docker
        state: started
        enabled: yes

    - name: Check if Jenkins container exists
      community.docker.docker_container_info:
        name: "{{ container_name }}"
      register: jenkins_container_info
      ignore_errors: true
      retries: 5
      delay: 5
      until: jenkins_container_info is not failed

    - name: Set flag if Jenkins container does not exist
      set_fact:
        jenkins_container_does_not_exist: "{{ jenkins_container_info.containers is not defined or jenkins_container_info.containers | length == 0 }}"

    - name: Pull Jenkins Docker image
      community.docker.docker_image:
        name: "{{ docker_image }}"
        source: pull
      when: jenkins_container_does_not_exist

    - name: Create Jenkins volume
      community.docker.docker_volume:
        name: jenkins_volume
        state: present
      when: jenkins_container_does_not_exist

    - name: Run Jenkins container
      community.docker.docker_container:
        name: "{{ container_name }}"
        image: "{{ docker_image }}"
        state: started
        restart_policy: always
        published_ports:
          - "{{ ports[0] }}"
          - "{{ ports[1] }}"
        volumes:
          - "jenkins_volume:{{ jenkins_home }}"
          - "/var/run/docker.sock:/var/run/docker.sock" # Bind mount the Docker socket
        env:
          CASC_JENKINS_CONFIG: "/var/jenkins_home/casc_configs/"
      when: jenkins_container_does_not_exist

    - name: Ensure Docker container is running
      community.docker.docker_container:
        name: "{{ container_name }}"
        state: started
      register: container_status

    - name: Wait for Jenkins to be ready
      uri:
        url: "http://{{ ansible_host }}:8080/login"
        method: GET
        return_content: yes
      register: jenkins_response
      until: jenkins_response.status == 200
      retries: 5
      delay: 10
      changed_when: false
      when: container_status.container.State.Running

    - name: Install Jenkins plugins
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: jenkins-plugin-cli --plugins {{ jenkins_plugins | join(' ') }}
      register: plugin_installation
      failed_when:
        - plugin_installation.rc != 0
      when: container_status.container.State.Running

    - name: Update package lists
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get update
      register: update_packages_result
      failed_when:
        - update_packages_result.rc != 0

    - name: Install basic utilities
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get install -y wget tar curl apt-transport-https ca-certificates gnupg2 software-properties-common
      register: install_result
      failed_when:
        - install_result.rc != 0

    - name: Check if Docker CLI is already installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: docker --version
      register: check_docker_cli_installed
      ignore_errors: true

    - name: Download Docker GPG key
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /tmp/docker.gpg
      register: download_docker_gpg_key
      failed_when:
        - download_docker_gpg_key.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Add Docker GPG key
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-key add /tmp/docker.gpg
      register: add_docker_gpg_key
      failed_when:
        - add_docker_gpg_key.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Add Docker repository
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: bash -c "echo 'deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable' > /etc/apt/sources.list.d/docker.list"
      register: add_docker_repository
      failed_when:
        - add_docker_repository.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Update package lists after adding Docker repository
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get update
      register: update_package_lists
      failed_when:
        - update_package_lists.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Install Docker CLI
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get install -y docker-ce-cli
      register: install_docker_cli_result
      failed_when:
        - install_docker_cli_result.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Create Docker group if it doesn't exist
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: groupadd -f -g 119 docker
      register: create_docker_group_result
      failed_when:
        - create_docker_group_result.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Add Jenkins user to Docker group
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: usermod -aG docker jenkins
      register: add_user_to_group_result
      failed_when:
        - add_user_to_group_result.rc != 0
      when: check_docker_cli_installed.rc != 0

    - name: Check if Node.js is installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: node -v
      register: check_node_installed
      ignore_errors: true

    - name: Check if npm is installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: npm -v
      register: check_npm_installed
      ignore_errors: true

    - name: Set node_not_installed variable
      set_fact:
        node_not_installed: "{{ check_node_installed.rc != 0 }}"

    - name: Set npm_not_installed variable
      set_fact:
        npm_not_installed: "{{ check_npm_installed.rc != 0 }}"

    - name: Download NodeSource setup script
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl -sL https://deb.nodesource.com/setup_20.x -o nodesource_setup.sh
      register: download_nodesource_script
      failed_when:
        - download_nodesource_script.rc != 0
      when: node_not_installed or npm_not_installed

    - name: Run NodeSource setup script
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: bash nodesource_setup.sh
      register: run_nodesource_script
      failed_when:
        - run_nodesource_script.rc != 0
      when: node_not_installed or npm_not_installed

    - name: Install Node.js and npm
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get install -y nodejs
      register: install_nodejs_npm
      failed_when:
        - install_nodejs_npm.rc != 0
      when: node_not_installed or npm_not_installed

    - name: Check if Python3 is installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: python3 --version
      register: check_python_installed
      ignore_errors: true

    - name: Check if pip3 is installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: pip3 --version
      register: check_pip_installed
      ignore_errors: true

    - name: Set python_not_installed variable
      set_fact:
        python_not_installed: "{{ check_python_installed.rc != 0 }}"

    - name: Set pip_not_installed variable
      set_fact:
        pip_not_installed: "{{ check_pip_installed.rc != 0 }}"

    - name: Install Python3 and pip3
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get install -y python3 python3-pip
      register: install_python_result
      failed_when:
        - install_python_result.rc != 0
      when: python_not_installed or pip_not_installed

    - name: Create symlink for python
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: ln -sf /usr/bin/python3 /usr/bin/python
      register: symlink_python_result
      failed_when:
        - symlink_python_result.rc != 0
      when: check_python_installed.rc == 0 # when python is installed

    - name: Check if AWS CLI is already installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: aws --version
      register: check_aws_cli_result
      ignore_errors: true

    - name: Download AWS CLI
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip'
      register: download_aws_cli_result
      failed_when:
        - download_aws_cli_result.rc != 0
      when: check_aws_cli_result.rc != 0

    - name: Unzip AWS CLI
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: unzip awscliv2.zip
      register: unzip_aws_cli_result
      failed_when:
        - unzip_aws_cli_result.rc != 0
      when: check_aws_cli_result.rc != 0

    - name: Install AWS CLI
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: ./aws/install
      register: install_aws_cli_result
      failed_when:
        - install_aws_cli_result.rc != 0
      when: check_aws_cli_result.rc != 0

    - name: Clean up installation files
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: rm -rf awscliv2.zip aws
      register: cleanup_result
      failed_when:
        - cleanup_result.rc != 0
      when: check_aws_cli_result.rc != 0

    - name: Check if Helm is already installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: helm version
      register: check_helm_installed
      ignore_errors: true

    - name: Download Helm signing key
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl -fsSL -o /tmp/helm-signing.asc https://baltocdn.com/helm/signing.asc
      register: download_helm_signing_key
      failed_when:
        - download_helm_signing_key.rc != 0
      when: check_helm_installed.rc != 0

    - name: Add Helm signing key
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-key add /tmp/helm-signing.asc
      register: add_helm_signing_key
      failed_when:
        - add_helm_signing_key.rc != 0
      when: check_helm_installed.rc != 0

    - name: Add Helm repository
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: bash -c "echo 'deb https://baltocdn.com/helm/stable/debian/ all main' | tee /etc/apt/sources.list.d/helm-stable-debian.list"
      register: add_helm_repo
      failed_when:
        - add_helm_repo.rc != 0
      when: check_helm_installed.rc != 0

    - name: Update package lists
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get update
      register: update_package_lists
      failed_when:
        - update_package_lists.rc != 0
      when: check_helm_installed.rc != 0

    - name: Install Helm
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: apt-get install -y helm
      register: install_helm
      failed_when:
        - install_helm.rc != 0
      when: check_helm_installed.rc != 0

    - name: Check if Helmfile is already installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: helmfile --version
      register: check_helmfile_installed
      ignore_errors: true

    - name: Download Helmfile
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl -Lo /usr/local/bin/helmfile https://github.com/roboll/helmfile/releases/download/v0.143.0/helmfile_linux_amd64
      register: download_helmfile
      failed_when:
        - download_helmfile.rc != 0
      when: check_helmfile_installed.rc != 0

    - name: Make Helmfile executable
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: chmod +x /usr/local/bin/helmfile
      register: make_helmfile_executable
      failed_when:
        - make_helmfile_executable.rc != 0
      when: check_helmfile_installed.rc != 0

    - name: Check if kubectl is already installed
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: kubectl version --client
      register: check_kubectl_installed
      ignore_errors: true

    - name: Get latest stable version of kubectl
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl -L -s https://dl.k8s.io/release/stable.txt
      register: get_kubectl_version
      failed_when:
        - get_kubectl_version.rc != 0
      when: check_kubectl_installed.rc != 0

    - name: Download kubectl
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: curl -LO "https://dl.k8s.io/release/{{ get_kubectl_version.stdout }}/bin/linux/amd64/kubectl"
      register: download_kubectl
      failed_when:
        - download_kubectl.rc != 0
      when: check_kubectl_installed.rc != 0

    - name: Install kubectl
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: install -o jenkins -g jenkins -m 0755 kubectl /usr/local/bin/kubectl
      register: install_kubectl
      failed_when:
        - install_kubectl.rc != 0
      when: check_kubectl_installed.rc != 0

    - name: Clean up kubectl file
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: rm -f kubectl
      register: cleanup_kubectl
      failed_when:
        - cleanup_kubectl.rc != 0
      when: check_kubectl_installed.rc != 0

    - name: Verify Node.js installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: node -v
      register: verify_node
      failed_when:
        - verify_node.rc != 0

    - name: Verify npm installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: npm -v
      register: verify_npm
      failed_when:
        - verify_npm.rc != 0

    - name: Verify Docker installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: docker --version
      register: verify_docker
      failed_when:
        - verify_docker.rc != 0

    - name: Verify Python installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: python --version
      register: verify_python
      failed_when:
        - verify_python.rc != 0

    - name: Verify AWS CLI installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: aws --version
      register: verify_aws
      failed_when:
        - verify_aws.rc != 0

    - name: Verify Helm installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: helm version
      register: verify_helm
      failed_when:
        - verify_helm.rc != 0

    - name: Verify Helmfile installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: helmfile --version
      register: verify_helmfile
      failed_when:
        - verify_helmfile.rc != 0

    - name: Verify kubectl installation
      community.docker.docker_container_exec:
        container: "{{ container_name }}"
        user: root
        command: kubectl version --client
      register: verify_kubectl
      failed_when:
        - verify_kubectl.rc != 0

    - name: Template Jenkins JCasC configuration
      ansible.builtin.template:
        src: jenkins-jcasc.yml.j2
        dest: /tmp/jenkins-jcasc.yml
        mode: "0640"
      no_log: true # Prevents logging of sensitive data

    - name: Ensure JCasC configuration directory exists in the Jenkins container
      community.docker.docker_container_exec:
        container: jenkins
        command: mkdir -p /var/jenkins_home/casc_configs

    - name: Copy JCasC configuration file into the Jenkins container
      community.docker.docker_container_copy_into:
        path: /tmp/jenkins-jcasc.yml
        container_path: /var/jenkins_home/casc_configs/jenkins-jcasc.yml
        container: jenkins

    - name: Change ownership of the JCasC configuration file in the container
      community.docker.docker_container_exec:
        container: jenkins
        command: chown jenkins:jenkins /var/jenkins_home/casc_configs/jenkins-jcasc.yml
      notify: restart docker

  handlers:
    - name: restart docker
      service:
        name: docker
        state: restarted
